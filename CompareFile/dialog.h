#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QFile>
#include <QDebug>
#include <iostream>
#include <string>
#include <QMessageBox>
#include <Windows.h>
#include <QtConcurrent/QtConcurrentRun>
using namespace std;
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    DWORD CalcPercent(int value,DWORD FileSize){ return (value*FileSize)/100;}
    void ShowSymbol(char *FileMas1,char *FileMas2);
private slots:
    void on_ButtonBrowseFile1_clicked();

    void on_ButtonBrowseFile2_clicked();

    void on_CompareButton_clicked();

    void on_lineEditFile1_textEdited(const QString &arg1);

    void on_lineEditFile2_textEdited(const QString &arg1);

    void on_scrollBar_valueChanged(int value);


signals:
    void UpdateBaseWindow1(QString);
    void UpdateBaseWindow2(QString);
    void UpdateFontColorWindow1(QColor);
    void UpdateFontColorWindow2(QColor);
private:
    Ui::Dialog *ui;
    QString PathFile1,PathFile2;//переменные для хранения пути
    HANDLE hFile1,hFile2,hMapFile1,hMapFile2;
    DWORD FileSize1,FileSize2,SizeReadBlock;
    char *BufferHexFile1,*BufferHexFile2;
    int count;
};

#endif // DIALOG_H
