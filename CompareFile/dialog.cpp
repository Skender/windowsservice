#include "dialog.h"
#include "ui_dialog.h"
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    PathFile1 = "";
    PathFile2 = "";
    SizeReadBlock = 66560;
    BufferHexFile1 = new char[SizeReadBlock+1];
    BufferHexFile2 = new char[SizeReadBlock+1];
    count = 0;
    ui->scrollBar->setMinimum(0);
    ui->scrollBar->setMaximum(100);
    connect(ui->textBrowserFile1->verticalScrollBar(),SIGNAL(valueChanged(int)),ui->textBrowserFile2->verticalScrollBar(),SLOT(setValue(int)));
    connect(ui->textBrowserFile2->verticalScrollBar(),SIGNAL(valueChanged(int)),ui->textBrowserFile1->verticalScrollBar(),SLOT(setValue(int)));
    connect (this,SIGNAL(UpdateBaseWindow1(QString)),ui->textBrowserFile1,SLOT(setText(QString)));
    connect (this,SIGNAL(UpdateBaseWindow2(QString)),ui->textBrowserFile2,SLOT(setText(QString)));
    connect(this,SIGNAL(UpdateFontColorWindow1(QColor)),ui->textBrowserFile1,SLOT(setTextColor(QColor)));
    connect(this,SIGNAL(UpdateFontColorWindow2(QColor)),ui->textBrowserFile2,SLOT(setTextColor(QColor)));
}

Dialog::~Dialog()
{

    CloseHandle(hMapFile1);
    CloseHandle(hMapFile2);
    CloseHandle(hFile1);
    CloseHandle(hFile2);
    delete BufferHexFile1,BufferHexFile2;
    delete ui;
}
/* Получение путей к файлам*/
//Получение пути к первому файлу по нажатию кнопки
void Dialog::on_ButtonBrowseFile1_clicked()
{
    ui->lineEditFile1->clear();
    PathFile1 = QFileDialog::getOpenFileName(0,QObject::tr("Укажите файл базы данных"),QDir::homePath(), QObject::tr("Все файлы (*.*)"));//открытие окна обозревателя
    if(PathFile1!=NULL)
        {
            hFile1=CreateFile(PathFile1.toStdWString().c_str(),GENERIC_READ,FILE_SHARE_READ,NULL,
                       OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_NO_BUFFERING,NULL);

            hMapFile1 = CreateFileMapping(hFile1,NULL,PAGE_READONLY,0,0,0);
            FileSize1 = GetFileSize(hMapFile1,0);
            ui->lineEditFile1->insert(PathFile1);// отображение пути выбранного файла
        }

}
//Получение пути ко второму файлу по нажатию кнопки
void Dialog::on_ButtonBrowseFile2_clicked()
{
    ui->lineEditFile2->clear();
    PathFile2 = QFileDialog::getOpenFileName(0,QObject::tr("Укажите файл базы данных"),QDir::homePath(), QObject::tr("Все файлы (*.*)"));
    if(PathFile2!=NULL)
        {
            hFile2=CreateFile(PathFile2.toStdWString().c_str(),GENERIC_READ,FILE_SHARE_READ,NULL,
                       OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_NO_BUFFERING,NULL);
            hMapFile2 = CreateFileMapping(hFile2,NULL,PAGE_READONLY,0,0,0);
            FileSize2 = GetFileSize(hMapFile2,0);
            ui->lineEditFile2->insert(PathFile2);// отображение пути выбранного файла
        }

}
// Для консольного ввода текста
void Dialog::on_lineEditFile1_textEdited(const QString &arg1)
{
    if(PathFile1==NULL)
        {
            PathFile1 = arg1;
            hFile1=CreateFile(PathFile1.toStdWString().c_str(),GENERIC_READ,FILE_SHARE_READ,NULL,
                       OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_NO_BUFFERING,NULL);

            hMapFile1 = CreateFileMapping(hFile1,NULL,PAGE_READONLY,0,0,0);
            FileSize1 = GetFileSize(hMapFile1,0);
        }

}

void Dialog::on_lineEditFile2_textEdited(const QString &arg1)
{
    if(PathFile2==NULL)
        {
            PathFile2 = arg1;
            hFile2=CreateFile(PathFile2.toStdWString().c_str(),GENERIC_READ,FILE_SHARE_READ,NULL,
                       OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_NO_BUFFERING,NULL);

            hMapFile2 = CreateFileMapping(hFile2,NULL,PAGE_READONLY,0,0,0);
            FileSize2 = GetFileSize(hMapFile2,0);
        }
}
/*Блок обработки полученного текста*/
void Dialog::on_CompareButton_clicked()
{
    on_scrollBar_valueChanged(0);
}




void Dialog::on_scrollBar_valueChanged(int value)
{
    /*Data for scroll*/

    ui->textBrowserFile1->verticalScrollBar()->setValue(value);
    ui->textBrowserFile2->verticalScrollBar()->setValue(value);

    DWORD FileOffset1 = CalcPercent(value,FileSize1);
    DWORD FileOffset2 = CalcPercent(value,FileSize2);


    char *LpAdressFile1 = (char*)MapViewOfFile(hMapFile1,FILE_MAP_READ,FileOffset1,
                                               FileOffset1,SizeReadBlock);
    char *LpAdressFile2 = (char*)MapViewOfFile(hMapFile2,FILE_MAP_READ,FileOffset2,
                                               FileOffset2,SizeReadBlock);
    if((!LpAdressFile1)||(!LpAdressFile2))
    {
        qDebug()<<"Error Memory";
        exit(0);
    }
    memcpy(BufferHexFile1,LpAdressFile1,SizeReadBlock);
    memcpy(BufferHexFile2,LpAdressFile2,SizeReadBlock);
    ui->textBrowserFile1->clear();
    ui->textBrowserFile2->clear();



    //sprintf_s(BufferHexFile1,SizeReadBlock,"%02x",LpAdressFile1,SizeReadBlock);
   // sprintf_s(BufferHexFile2,SizeReadBlock,"%02x",LpAdressFile2,SizeReadBlock);


    QFuture<void> future = QtConcurrent::run<void>(this,&Dialog::ShowSymbol,BufferHexFile1,BufferHexFile2);
    future.waitForFinished();
    future.cancel();
    UnmapViewOfFile((LPCVOID)LpAdressFile1);
    UnmapViewOfFile((LPCVOID)LpAdressFile2);
    //Control data File

}

void Dialog::ShowSymbol(char *FileMas1, char *FileMas2)
{
    int index = 0;
    while((index<strlen(FileMas1))||(index<strlen(FileMas2)))
        {
            if(count==8)
                {
                    count=0;
                    emit UpdateBaseWindow1("\n");
                    emit UpdateBaseWindow2("\n");
                }
            if(FileMas1[index]==FileMas2[index])
                {
                    emit UpdateBaseWindow1((QString)FileMas1[index] + "    ");
                    emit UpdateBaseWindow2((QString)FileMas2[index] + "    ");
                }
            else
                {
                    emit UpdateFontColorWindow1(QColor("red"));
                    emit UpdateFontColorWindow2(QColor("red"));

                    emit UpdateBaseWindow1((QString)FileMas1[index] + "    ");
                    emit UpdateBaseWindow2((QString)FileMas2[index] + "    ");

                    emit UpdateFontColorWindow1(QColor("black"));
                    emit UpdateFontColorWindow2(QColor("black"));

                }
            count++;
            index++;
        }
}





