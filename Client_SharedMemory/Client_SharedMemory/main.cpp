#include	<iostream>
#include	<Windows.h>
#include	<locale>
#include	<conio.h>
using namespace std;

int main()
{
	bool FlagInput;
	bool FlagSelect;
	char Symbol;
	setlocale(LC_ALL,"Ru");
	string StringStr = "";
	HANDLE hMapSharedMemmoryString , Event;
	DWORD BufSize = 1024*1024;
	TCHAR TCharMessageMemoryShared[] = TEXT("MessageSharedMemory");
	LPCTSTR pAdress = NULL;
	hMapSharedMemmoryString = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE,TCharMessageMemoryShared);
	if(hMapSharedMemmoryString==NULL)
		{
			hMapSharedMemmoryString = CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,BufSize,TCharMessageMemoryShared);
			if(hMapSharedMemmoryString==NULL)
				{
					cout<<"Error Create File Mapping = "<<GetLastError()<<endl;
					return 1;
				}
		}
	Event = CreateEvent(NULL,true,false,(LPCWSTR)"SaveMe");
		if(Event == NULL)
		{
			cout<<"Error Create Event = "<<GetLastError()<<endl;
			CloseHandle(hMapSharedMemmoryString);
			return 1;
		}
	do
	{
		system("cls");
		FlagInput = FlagSelect = true;
		pAdress = (LPTSTR)MapViewOfFile(hMapSharedMemmoryString,FILE_MAP_ALL_ACCESS,0,0,BufSize);
		if(pAdress==NULL)
			{
				cout<<"Error Map View Of File = "<<GetLastError()<<endl;
				return 1;
				CloseHandle(hMapSharedMemmoryString);
			}

		cout<<"����������, ������� ��������� ��� ����� ������ : ";
		cin>>StringStr;
		
		CopyMemory((PVOID)pAdress,StringStr.c_str(),(StringStr.size()*sizeof(string)));
		PulseEvent(Event);


		system("cls");
		while(FlagSelect)
			{

				cout<<"��������� ����?!(Y|N)"<<endl;
				Symbol = _getch();
				if((Symbol=='Y')||(Symbol=='y'))
					{
						FlagSelect = false;
						StringStr = "";
						UnmapViewOfFile(pAdress);

					}
				else 
				if((Symbol=='N')||(Symbol=='n'))
					{
						FlagInput = FlagSelect = false;
					}
			}
	}while (FlagInput);

	UnmapViewOfFile(pAdress);
	CloseHandle(hMapSharedMemmoryString);
	CloseHandle(Event);
	cout<<"�� �������� ��� �������� �����..... :-("<<endl;
	_getch();
	
	return 0;
}