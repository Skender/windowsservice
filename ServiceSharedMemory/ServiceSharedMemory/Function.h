#include <ctime>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <process.h>
#include <tchar.h>
#include <string>
#include <locale>
#using <System.dll>
#using <mscorlib.dll>
using namespace System;
using namespace System::Diagnostics;
using namespace std;
int StartService();
int InstallService();
int RemoveService();
void ServiceSharedMemoryDateTime(char *buffer)
{
	time_t seconds = time(NULL);
	tm *timeinfo = localtime(&seconds);
	char* format = "%d_%d_%Y_%I_%M_%S";
	strftime(buffer, 80, format, timeinfo);
}
int FullPathOut(char *buffer,char *message)
{
	char *FullPath = new char[strlen(buffer)+strlen(".txt")+1];
	strcpy(FullPath,buffer);
	strcat(FullPath,".txt");

	ofstream outstream(FullPath);
	if(outstream.fail())
	{
		cout<<"No create File"<<endl;
		return 1;
	}
	outstream<<buffer<<endl;
	outstream<<message<<endl;
	
	outstream.close();
	return 0;
}
void ServiceSharedMemoryEventLog(char *buffer,char *message)
{
	String ^sSource;
	String ^sLog;
	String ^sEvent;
	sSource = gcnew String(message);
	sLog = gcnew String("SHAREDMEMORYSERVICE");
	sEvent = gcnew String(buffer);

	 if(!EventLog::SourceExists(sSource))
		 {
			EventLog::CreateEventSource(sSource,sLog);
		 }
	EventLog::WriteEntry(sSource,sEvent);
	EventLog::WriteEntry(sSource, sEvent,EventLogEntryType::Warning, 234);
}
int EventMessage()
{
	char buffer[80];
	HANDLE hMapClientMessage,*ServiceEvent = new HANDLE;
	char* pAdress=NULL;
	DWORD BufSize = 1024*1024,dwWaitResult;
	TCHAR TCharMessageMemoryShared[] = TEXT("MessageSharedMemory");
	setlocale(LC_ALL,"Ru");
	hMapClientMessage = CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,BufSize,TCharMessageMemoryShared);
	if(hMapClientMessage==NULL)
		{
			cout<<"Error Create File Mapping = "<<GetLastError()<<endl;
			return 1;
		}

	while (true)
		{
			pAdress =(char*) MapViewOfFile(hMapClientMessage,FILE_MAP_ALL_ACCESS,0,0,BufSize);
			if(pAdress == NULL)
				{
					cout<<"Error Map View Of File = "<<GetLastError()<<endl;
					CloseHandle(hMapClientMessage);
					return 1;
				}
			*ServiceEvent = OpenEvent(EVENT_ALL_ACCESS,FALSE,(LPCWSTR)"SaveMe");
			if(ServiceEvent==NULL)
				{
					cout<<"Error Open Event = "<<GetLastError()<<endl;
					CloseHandle(hMapClientMessage);
					return 1;
				}
			dwWaitResult = WaitForMultipleObjects(1,ServiceEvent,FALSE,INFINITE);
			if(dwWaitResult == WAIT_OBJECT_0)
				{
					
									
					ServiceSharedMemoryDateTime(buffer);
					cout<<buffer<<endl;
					cout<<"Message For Service : "<<pAdress<<endl;
					FullPathOut(buffer,pAdress);
					ServiceSharedMemoryEventLog(buffer,pAdress);
					UnmapViewOfFile(pAdress);
				}
			else
				{
					UnmapViewOfFile(pAdress);
				}
			ResetEvent(ServiceEvent);
		}
	CloseHandle(hMapClientMessage);
	CloseHandle(ServiceEvent);
	UnmapViewOfFile(pAdress);
	delete ServiceEvent;
	return 0;
}