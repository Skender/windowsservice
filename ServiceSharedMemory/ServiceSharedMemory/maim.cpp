#include "Function.h"
#include <strsafe.h>
SERVICE_STATUS g_ServiceStatus = {0};
SERVICE_STATUS_HANDLE g_StatusHandle = NULL;
HANDLE g_ServiceStopEvent = INVALID_HANDLE_VALUE;
void WINAPI ServiceMain(DWORD argc,LPTSTR *argv);
void WINAPI ServiceCtrlHandler(DWORD);
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam);
int InstallService(TCHAR *ServicePath);
int RemoveService();
int MyStartService();
#define SERVICE_NAME _T("SharedMemoryService")
int _tmain(int argc,TCHAR *argv[])
{
	TCHAR *ServicePath = LPTSTR(argv[0]);
	SERVICE_TABLE_ENTRY ServiceTable[] = 
	{
		{SERVICE_NAME,(LPSERVICE_MAIN_FUNCTION) ServiceMain},
		{NULL,NULL}
	};
	if(StartServiceCtrlDispatcher(ServiceTable) == false)
	{
		return GetLastError();
	}
	 else 
		 if( wcscmp(argv[argc-1], _T("install")) == 0) 
		 {
			InstallService(ServicePath);
		 } 
		 else 
			 if( wcscmp(argv[argc-1], _T("remove")) == 0) 
			 {
				RemoveService();
			 } 
			 else 
				 if( wcscmp(argv[argc-1], _T("start")) == 0 )
				 {
					MyStartService();
					EventMessage();
			     }
	return 0;	
}
void WINAPI ServiceMain(DWORD argc,LPTSTR *argv)
{
	DWORD Status = E_FAIL;
	g_StatusHandle = RegisterServiceCtrlHandler(SERVICE_NAME,ServiceCtrlHandler);
	if(g_StatusHandle == NULL)
	{
		return;
	}
	ZeroMemory(&g_ServiceStatus,sizeof(g_ServiceStatus));
	g_ServiceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	g_ServiceStatus.dwControlsAccepted = 0;
	g_ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
	g_ServiceStatus.dwWin32ExitCode = 0;
	g_ServiceStatus.dwServiceSpecificExitCode = 0;
	g_ServiceStatus.dwCheckPoint = 0;
	if(SetServiceStatus(g_StatusHandle,&g_ServiceStatus)==NULL)
	{
		OutputDebugString(_T("SetServiceStarus Returned Error"));
	}
	
	g_ServiceStopEvent = CreateEvent(NULL,TRUE,FALSE,NULL);
	if(g_ServiceStopEvent == NULL)
	{
		g_ServiceStatus.dwControlsAccepted = 0;
		g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
		g_ServiceStatus.dwWin32ExitCode = GetLastError();
		g_ServiceStatus.dwCheckPoint = 1;
		if(SetServiceStatus(g_StatusHandle,&g_ServiceStatus)==FALSE)
		{
			OutputDebugString(_T("SetServiceStarus Returned Error"));
		}
		return;
	}
	
	g_ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
	g_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
	g_ServiceStatus.dwWin32ExitCode = 0;
	g_ServiceStatus.dwCheckPoint = 0;
	if(SetServiceStatus(g_StatusHandle,&g_ServiceStatus)==FALSE)
		{
			OutputDebugString(_T("SetServiceStarus Returned Error"));
		}

	HANDLE hThread = CreateThread(NULL,0,ServiceWorkerThread,NULL,0,NULL);
	WaitForSingleObject(hThread,INFINITE);
	CloseHandle(g_ServiceStopEvent);

	g_ServiceStatus.dwControlsAccepted = 0;
	g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
	g_ServiceStatus.dwWin32ExitCode = 0;
	g_ServiceStatus.dwCheckPoint = 3;
		if(SetServiceStatus(g_StatusHandle,&g_ServiceStatus)==FALSE)
		{
			OutputDebugString(_T("SetServiceStarus Returned Error"));
		}
}
void WINAPI ServiceCtrlHandler(DWORD CtrlCode)
{
	switch (CtrlCode)
	{
	case SERVICE_CONTROL_STOP:
		if(g_ServiceStatus.dwCurrentState != SERVICE_RUNNING){break;}
		g_ServiceStatus.dwControlsAccepted = 0;
		g_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		g_ServiceStatus.dwWin32ExitCode = 0;
		g_ServiceStatus.dwCheckPoint = 4;
		if(SetServiceStatus(g_StatusHandle,&g_ServiceStatus)==FALSE)
		{
			OutputDebugString(_T("SetServiceStarus Returned Error"));
		}
		SetEvent(g_ServiceStopEvent);
		break;
	default:
		break;
	}
}
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam)
{
	while(WaitForSingleObject(g_ServiceStopEvent,0)!=WAIT_OBJECT_0)
	{
		Sleep(3000);	
	}
	return ERROR_SUCCESS;
}

int InstallService(TCHAR *ServicePath)
{
	SC_HANDLE hScManager = OpenSCManager(NULL,NULL,SC_MANAGER_CREATE_SERVICE);
	if(!hScManager)
	{
		cout<<GetLastError();
		return -1;
	}
	SC_HANDLE hService = CreateService(
		hScManager,
		SERVICE_NAME,
		SERVICE_NAME,
		SERVICE_ALL_ACCESS,
		SERVICE_WIN32_OWN_PROCESS,
		SERVICE_DEMAND_START,
		SERVICE_ERROR_NORMAL,
		ServicePath,
		NULL,NULL,NULL,NULL,NULL
		);
	if(!hService)
	{
		cout<<GetLastError();
		CloseServiceHandle(hScManager);
		return -1;
	}
	CloseServiceHandle(hService);
	CloseServiceHandle(hScManager);
	return 0;
}
int RemoveService()
{
	SC_HANDLE hScManager = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);
	if(!hScManager)
	{
		cout<<GetLastError();
		return -1;
	}
	SC_HANDLE hService = OpenService(hScManager,SERVICE_NAME,SERVICE_STOP|DELETE);
	if(!hService)
	{
		cout<<GetLastError();
		CloseServiceHandle(hScManager);
		return -1;
	}
	DeleteService(hService);
	CloseServiceHandle(hService);
	CloseServiceHandle(hScManager);
	return 0;
}
int MyStartService()
{
	SC_HANDLE hScManager = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);
	SC_HANDLE hService = OpenService(hScManager,SERVICE_NAME,SERVICE_START);
	if(!StartService(hService,0,NULL))
	{
		cout<<GetLastError();
		CloseServiceHandle(hScManager);
		return -1;
	}
	CloseServiceHandle(hService);
	CloseServiceHandle(hScManager);
	return 0;
}
